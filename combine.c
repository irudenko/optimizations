#include <stdio.h>
#include "vec.h"

#define LEN   1024


long rdtscp(int* cpuid);

/* Combine
 * v     is a non-NULL pointer to a vec
 * dest  is an out parameter that will receive the application of OP to the
 *       elements of v
 */
void combine1(vec_ptr v, data_t *dest) {

    *dest = IDENT;
    for(size_t i = 0; i < vec_length(v); ++i) {
        data_t val;
        get_vec_element(v, i, &val);
        *dest = *dest OP val;
    }
}
void combine2(vec_ptr v, data_t *dest) {

    *dest = IDENT;
    size_t len = vec_length(v);
    for(size_t i = 0; i < len; ++i) {
        data_t val;
        get_vec_element(v, i, &val);
        *dest = *dest OP val;
    }
}

void combine3(vec_ptr v, data_t *dest) {

  data_t acc = IDENT;
  size_t len = vec_length(v);
  for(size_t i = 0; i < len; ++i) {
    data_t val;
    get_vec_element(v, i, &val);
    acc = acc OP val;
  }
  *dest = acc;
}
void combine4(vec_ptr v, data_t *dest) {
  size_t len = v->len;
  data_t acc = IDENT;
    for(size_t i = 0; i < len; ++i) {
      acc = acc OP v->data[i];
    }
    *dest = acc;
}    

/* Simple test program for combine1.  
 *
 */
int main() {
    vec_ptr v = new_vec(LEN);

    for(size_t i = 0; i < LEN; ++i) {
      set_vec_element(v, i, (data_t) i);
    }

    int  cpustart;
    long start = rdtscp(&cpustart);
    data_t dest;
    func(v, &dest);
    int  cpuend;
    long end = rdtscp(&cpuend);

    
    int reslt = (int) dest;
    int check = (LEN * (LEN-1))/2;
    if(reslt != check)
        printf("Check failed %d != %d\n", reslt, check);

    printf("cpustart = %d ", cpustart);
    printf("cpuend   = %d ", cpuend);
    printf("cycles   = %lu\n", end - start);

    return reslt != check ? EXIT_FAILURE: EXIT_SUCCESS;
}
