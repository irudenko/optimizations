#include "vec.h"


/* New Vec
 *
 * There are two allocations here: the vec_rec and the array of data_t.  Either
 * could fail.  There is a bit of clean-up needed if the vec_rec could be
 * allocated, but the array could not.  
 *
 * If the requested length is zero, the data filed is NULL.  There really isn't
 * a point to allocating an array of length zero.  
 */
vec_ptr new_vec(size_t len){
    
    vec_ptr v = malloc(sizeof(vec_rec));
    if(v != NULL) {
        
        v->len = len;
        v->data = NULL;
        
        if(v->len > 0) {
            v->data = calloc(len, sizeof(data_t));
            if(v->data == NULL) {
                free(v);
                v = NULL;
            }
        }
        
    }
    return v;
}

/* Free Vec
 * 
 */
void free_vec(vec_ptr v){
    if(v != NULL) {
        free(v->data);
        free(v);
    }
}

/* Get Vec Element
 */
bool get_vec_element(vec_ptr v, size_t index, data_t* data){
    bool getable  = index < v->len;
    if(getable)
        *data = v->data[index];
    return getable;
}

/* Set Vec Element
 * v        A non-NULL pointer to the vector.
 * index    The index of the data to be replaced.
 * data     The data to copy to the specified index.
 * returns  True if the data at the specified index is replaced. 
 */
bool set_vec_element(vec_ptr v, size_t index, data_t data){
    bool setable = index < v->len;
    if(setable) 
        v->data[index] = data;
    return setable;
}

/* Vec Length
 * v        A non-NULL pointer to the vector.
 * returns  the capacity of v.  
 */
size_t vec_length(vec_ptr v) {
    return v->len;
}
