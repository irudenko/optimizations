#if !defined(_570d5968_4f73_4843_adc2_84ea50bcdddd)
#define _570d5968_4f73_4843_adc2_84ea50bcdddd

#include <stdlib.h>
#include <stdbool.h>

struct vec_rec {
    size_t  len;
    data_t *data;
};
typedef struct vec_rec  vec_rec;
typedef struct vec_rec* vec_ptr;

/* New Vec
 * len      The storage capacity of the new vector. 
 * returns  The newly allocated vec_rec or NULL if a vec_rec could not be
 *          allocated. 
 *
 * Creates and returns a new vector capable of storing at least len items. 
 */
vec_ptr new_vec(size_t len);

/* Free Vec
 * v   The vector to free or NULL. 
 *
 * Frees all allocated memory associated with v.  
 */
void free_vec(vec_ptr v);

/* Get Vec Element
 * v        A non-NULL pointer to the vector.
 * index    The index of the data to retrieve.
 * data     A non-Null pointer to receive a copy of the specified data. 
 * returns  True if the data at the specified index were successfully copied.
 */
bool get_vec_element(vec_ptr v, size_t index, data_t* data);

/* Set Vec Element
 * v        A non-NULL pointer to the vector.
 * index    The index of the data to be replaced.
 * data     The data to copy to the specified index.
 * returns  True if the data at the specified index is replaced. 
 */
bool set_vec_element(vec_ptr v, size_t index, data_t data);

/* Vec Length
 * v        A non-NULL pointer to the vector.
 * returns  the capacity of v.  
 */
size_t vec_length(vec_ptr v);



#endif 
